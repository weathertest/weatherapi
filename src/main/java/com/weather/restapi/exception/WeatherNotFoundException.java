package com.weather.restapi.exception;

public class WeatherNotFoundException extends RuntimeException {
        public WeatherNotFoundException(String city) {
            super("Could not retrieve weather for city " + city);
        }
}
