package com.weather.restapi.services;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.restapi.exception.WeatherNotFoundException;
import com.weather.restapi.models.Weather;
import com.weather.restapi.models.WeatherUrl;
import com.weather.restapi.repositories.WeatherRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.Optional;

@Service
public class WeatherService {

	private static final Logger logger = LoggerFactory.getLogger(WeatherService.class);

	@Autowired
	private WeatherUrl weatherData;

	@Autowired
	WeatherRepository weatherRepository;

	@Autowired
	RestTemplate restTemp;

	@Retryable(value = WeatherNotFoundException.class)
	public Weather getWeather(String city) {
		logger.debug("Getting weather report for city "+city);
		try {
			populateWeather(city);
		}catch(IOException ioe){
			logger.error("Network error  "+ioe);
		}
		return weatherRepository.findByName(city).orElseThrow(() -> new WeatherNotFoundException(city));
	}

	@Retryable(value = IOException.class)
	@Async
	public void populateWeather(String city) throws IOException {
		
		logger.debug("Getting weather report from web for city  "+city);

		UriComponents uriComponents = UriComponentsBuilder.newInstance().scheme("http").host(weatherData.getUrl())
				.path("").query("q={keyword}&appid={appid}").buildAndExpand(city, weatherData.getApiKey());

		String uri = uriComponents.toUriString();

		ResponseEntity<String> resp = restTemp.exchange(uri, HttpMethod.GET, null, String.class);

		ObjectMapper mapper = new ObjectMapper();

		Weather weatherDetails = mapper.readValue(resp.getBody(), Weather.class);
		logger.debug("Received weather report  "+weatherDetails);
		Optional<Weather> oldRec = weatherRepository.findByName(city);
		if (oldRec.isPresent()){
			weatherDetails.setId(oldRec.get().getId());
			
		}
			
		weatherRepository.save(weatherDetails);
		logger.debug("saved weather report in mongodb ");

	}

}
