package com.weather.restapi.controllers;


import java.io.IOException;

import com.weather.restapi.models.WeatherUrl;
import com.weather.restapi.services.WeatherService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.weather.restapi.models.Weather;

@ComponentScan("com.weather.restapi.config")
@RestController

public class WeatherController {
	private static final Logger logger = LoggerFactory.getLogger(WeatherController.class);


	@Autowired
	WeatherService weatherService;

	@CrossOrigin(origins = "http://localhost:5000")
	@GetMapping("/weather")
	Weather getWeather(@RequestParam String city) {
		logger.debug("Getting weather report for city "+city);
		return weatherService.getWeather(city);

	}

		
	}