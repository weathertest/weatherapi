package com.weather.restapi.repositories;

import com.weather.restapi.models.Weather;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface WeatherRepository extends MongoRepository<Weather, String> {

    public Optional<Weather> findByName(String city);
}
