package com.weather.restapi;



import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.weather.restapi")
@SpringBootApplication
public class WeatherApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(WeatherApplication.class, args);
	}
}