
package com.weather.report;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import com.weather.restapi.WeatherApplication;
import com.weather.restapi.models.Weather;
import com.weather.restapi.repositories.WeatherRepository;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WeatherApplication.class)
public class TestWebApp {

	@Autowired
      WeatherRepository weatherRepository;
	
	@Test
	public void testMongoDocumentCount() {
	
		Iterable<Weather> iterator = weatherRepository.findAll();
		Collection<Weather> data = new ArrayList<Weather>();
		for(Weather weather : iterator) {
			data.add(weather);
		}
		assertTrue(data.size()>=0);
	}
	
	@Test
	public void testWeatherAPI() {
	    UriComponents uriComponents = UriComponentsBuilder.newInstance()
	      .scheme("http").host("api.openweathermap.org/data/2.5/weather")
	      .path("").query("q={keyword}&appid={appid}").buildAndExpand("bangalore","1c9770dfaf3b327dd03510a4c07b7f2d");
	 
	     assertEquals("http://api.openweathermap.org/data/2.5/weather?q=bangalore&appid=1c9770dfaf3b327dd03510a4c07b7f2d", uriComponents.toUriString());

	}
}
	